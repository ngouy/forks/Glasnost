import React, {Component} from 'react';
import {TextInput, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {navigate, registerRoute} from 'router';
import {
    AppContainer, colors,
    FormattedText,
    MainButton,
    mainInputStyle,
    ScrollableAppContainer, SimpleInput
} from 'elements';

import {getRepository} from '../repository/selectors';
import {createNewIssue} from './actions';
import {requestMembers} from '../repository/actions';
import {Assignees} from './Assignees';

function Title({onChange}) {
    return (
        <View style={{flexDirection: 'row', marginVertical: 20}}>
            <SimpleInput onChangeText={onChange} style={{flex: 1}} placeholder={'Issue title'}/>
        </View>
    );
}

function Description({onChange}) {
    return (
        <View style={{flexDirection: 'row'}}>
            <TextInput
                multiline={true}
                textAlignVertical={'top'}
                onChangeText={onChange}
                style={{...mainInputStyle, height: 100, flex: 1}}
                placeholder={'Description'}
                placeholderTextColor={colors.mainText}
            />
        </View>
    );
}

function Header({repository}) {
    return (
        <View>
            <FormattedText style={{fontSize: 20, fontWeight: 'bold'}}>New issue for</FormattedText>
            <FormattedText
                style={{fontSize: 20, fontWeight: 'bold', fontStyle: 'italic'}}>{repository.name}</FormattedText>
        </View>
    );
}

function isIssueValid(title, description) {
    return !_.isEmpty(title) && !_.isEmpty(description);
}

class NewIssueComponent extends Component {
    state = {title: '', description: '', assignees: [], issueIsValid: false};

    componentDidMount() {
        this.props.requestMembers();
    }

    onMemberPress(member) {
        const {assignees} = this.state;

        if (!_.includes(assignees, member.id)) {
            this.setState({assignees: [...assignees, member.id]});
        }
    }

    onAvatarPress(member) {
        const {assignees} = this.state;

        if (_.includes(assignees, member.id)) {
            this.setState({assignees: _.filter(assignees, assignee => assignee !== member.id)});
        }
    }

    onTitleChange(title) {
        this.setState({title}, () => this.checkIssue());
    }

    onDescriptionChange(description) {
        this.setState({description}, () => this.checkIssue());
    }

    checkIssue() {
        const {title, description} = this.state;
        this.setState({issueIsValid: isIssueValid(title, description)});
    }

    render() {
        const {repository, createNewIssue, navigate} = this.props;
        const {title, description, assignees, issueIsValid} = this.state;

        if (_.isEmpty(repository)) {
            return <AppContainer/>;
        }

        return (
            <ScrollableAppContainer style={{paddingVertical: 10}}>
                <Header repository={repository}/>
                <Title onChange={title => this.onTitleChange(title)}/>
                <Description onChange={description => this.onDescriptionChange(description)}/>
                <Assignees
                    onPress={member => this.onMemberPress(member)}
                    onAvatarPress={member => this.onAvatarPress(member)}
                    assignees={assignees}
                />
                <View style={{
                    flex: 1, marginTop: 20, flexDirection: 'row', justifyContent: 'space-around'
                }}>
                    <MainButton text={'Dismiss'} secondary={true} onPress={() => navigate('Repository')}/>
                    <MainButton text={'Submit issue'} disabled={!issueIsValid} onPress={() => {
                        createNewIssue({title, description, assignees});
                        navigate('Repository');
                    }}/>
                </View>
            </ScrollableAppContainer>
        );
    }
}

const NewIssue = connect(state => ({repository: getRepository(state)}), {
    requestMembers,
    createNewIssue,
    navigate
})(NewIssueComponent);
registerRoute({NewIssue});
