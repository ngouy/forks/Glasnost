import _ from 'lodash';

export const getWikis = state => _.get(state, ['wikis', 'wikis']);
