import _ from 'lodash';

export const getTodos = state => _.get(state, ['todos', 'todos']);
