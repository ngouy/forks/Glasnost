import {getRegisteredRoutes} from 'router';

import * as index from '../index';

describe('Logout index', () => {
    it('should export Logout', () => {
        expect(JSON.stringify(index)).toEqual('{}');
        expect(getRegisteredRoutes().Logout).toBeDefined();
    });
});
