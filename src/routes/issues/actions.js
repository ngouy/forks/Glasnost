import _ from 'lodash';
import {get} from 'api';
import {getPagination, setPagination} from 'utils';
import {getRepository} from '../repository/selectors';

function requestStateIssues(personal, state, page0) {
    return async (dispatch, getState) => {
        let issues;

        const {pageToLoad = 0} = getPagination(state, 'issues');

        if (personal) {
            issues = await get({
                page: page0 ? 0 : pageToLoad,
                path: 'issues',
                additionalParams: {per_page: 100, scope: 'assigned_to_me', state},
                cached: !page0
            });
        } else {
            const repo = getRepository(getState());

            if (_.isEmpty(repo)) {
                return;
            }

            issues = await get({
                page: page0 ? 0 : pageToLoad,
                path: `projects/${repo.id}/issues`,
                additionalParams: {per_page: 100, state}
            });
        }

        if (issues.data) {
            dispatch({type: 'ISSUES_RECEIVED', data: issues.data});
            dispatch(setPagination('issues', issues.headers));
        }
    };
}

export function requestIssues(personal, page0) {
    return (dispatch) => {
        dispatch(requestStateIssues(personal, 'opened', page0));
        dispatch(requestStateIssues(personal, 'closed', page0));
    };
}

export function clearIssues() {
    return dispatch => dispatch({type: 'CLEAR_ISSUES'});
}
