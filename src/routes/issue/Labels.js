import {connect} from 'react-redux';
import {View} from 'react-native';
import React from 'react';
import _ from 'lodash';

import {Tag} from 'elements';

import {getIssue} from './selectors';

const containerStyle = {
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    marginVertical: 20,
    justifyContent: 'space-between'
};

function LabelsComponent({issue}) {
    const {labels} = issue;

    return (
        <View style={containerStyle}>
            {_.map(labels, (label, index) => <View style={{marginTop: 3}} key={index}><Tag text={label}/></View>)}
        </View>
    );
}

export const Labels = connect(state => ({issue: getIssue(state)}))(LabelsComponent);
