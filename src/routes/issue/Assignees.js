import React from 'react';
import {View} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';

import {navigate} from 'router';
import {CircledImage, FormattedText} from 'elements';

function AssigneesComponent({assignees, navigate}) {
    if (!Array.isArray(assignees)) {
        assignees = _.compact([assignees]);
    }

    const assigneesNumber = _.size(assignees);

    if (!assigneesNumber) {
        return null;
    }

    return (
        <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
            <FormattedText>{`Assignee${assigneesNumber > 1 ? 's' : ''} :`}</FormattedText>
            {_.map(assignees, assignee => <CircledImage
                onPress={() => navigate('Profile', {id: assignee.id})}
                key={assignee.name}
                name={assignee.name}
                url={assignee.avatar_url}
            />)}
        </View>
    );
}

export const Assignees = connect(null, {navigate})(AssigneesComponent);
