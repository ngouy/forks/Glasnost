import React, {Component} from 'react';
import {KeyboardAvoidingView, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {navigate, registerRoute} from 'router';
import {FormattedText,
    Markdown,
    Card,
    ScrollableAppContainer,
    AppContainer,
    CircledImage,
    colors
} from 'elements';
import {getBasicDateFormat, getLastActivity, getProjectPermissionLevel} from 'utils';
import {setFooter, setHeaderRightButton} from 'headerFooter';

import {getIssue} from './selectors';
import {editIssue, requestIssue} from './actions';
import {Discussions} from './Discussions';
import {getMe} from '../profile/selectors';
import {getRepository} from '../repository/selectors';
import {CommentSection} from './CommentSection';
import {Labels} from './Labels';
import {Assignees} from './Assignees';

class IssueComponent extends Component {
    state = {isCommentSectionVisibile: false};

    componentDidMount() {
        const {requestIssue, iid, projectId} = this.props;

        requestIssue(iid, projectId);
    }

    headerRightButton() {
        const {issue, me, repository, editIssue} = this.props;

        const permissions = _.get(repository, 'permissions', {});
        const {state, author} = issue;

        const project_access = _.get(permissions, 'project_access', {});
        const group_access = _.get(permissions, 'group_access', {});
        const authorId = _.get(author, 'id');
        const hasPermission = (authorId === _.get(me, 'id') || getProjectPermissionLevel(project_access, group_access) >= 20);
        const canClose = state === 'opened' && hasPermission;
        const canReopen = state === 'closed' && hasPermission;

        if (canClose) {
            return (
                <TouchableHighlight onPress={() => editIssue({state_event: 'close'})}>
                    <Text style={{color: colors.warning}}>{'Close Issue'}</Text>
                </TouchableHighlight>
            );
        }

        if (canReopen) {
            return (
                <TouchableHighlight onPress={() => editIssue({state_event: 'reopen'})}>
                    <FormattedText>{'Reopen'}</FormattedText>
                </TouchableHighlight>
            );
        }

        return null;
    }

    footer() {
        const {issue: {state}} = this.props;
        const {isCommentSectionVisibile} = this.state;

        if (state !== 'opened' || isCommentSectionVisibile) {
            return null;
        }

        return (
            <TouchableOpacity style={{flex: 1}} onPress={() => this.setState({isCommentSectionVisibile: true})}>
                <FormattedText style={{fontSize: 20, textDecorationLine: 'underline', textAlign: 'center'}}>{'Write a comment'}</FormattedText>
            </TouchableOpacity>
        );
    }

    render() {
        const {issue, navigate, requestIssue, setHeaderRightButton, setFooter, iid, projectId} = this.props;
        const {isCommentSectionVisibile} = this.state;

        if (_.isEmpty(issue)) {
            return <AppContainer/>;
        }

        setHeaderRightButton(this.headerRightButton());
        setFooter(this.footer());
        const {milestone} = issue;

        return (
            <View style={{flex: 1}}>
                <ScrollableAppContainer refresh={() => requestIssue(iid, projectId)}>
                    <KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={50} style={{flex: 1}}>
                        <Header issue={issue} navigate={navigate}/>
                        {milestone && <Card title={'Stats'} icon={{name: 'overview'}}>
                            <Milestone milestone={milestone}/>
                        </Card>}
                        <Description issue={issue}/>
                        {isCommentSectionVisibile
                        && <CommentSection onClose={() => this.setState({isCommentSectionVisibile: false})}/>}
                        <Discussions/>
                    </KeyboardAvoidingView>
                </ScrollableAppContainer>
            </View>
        );
    }
}

function Header({issue, navigate}) {
    const {title, updated_at, created_at, author} = issue;
    const authorName = _.get(author, 'name');
    const authorId = _.get(author, 'id');
    const authorUrl = _.get(author, 'avatar_url');
    const formattedLastActivity = getLastActivity(updated_at);
    const formattedCreatedAt = getLastActivity(created_at);
    const hasBeenUpdated = formattedCreatedAt !== formattedLastActivity;

    return (
        <View style={{alignItems: 'center', marginVertical: 10, marginHorizontal: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                <FormattedText style={{fontSize: 14, marginRight: 10}}>{`Opened: ${formattedCreatedAt} by`}</FormattedText>
                <CircledImage name={authorName} url={authorUrl} onPress={() => navigate('Profile', {id: authorId})}/>
            </View>
            {hasBeenUpdated && <FormattedText style={{fontSize: 14}}>{`Last activity: ${formattedLastActivity}`}</FormattedText>}
            <FormattedText style={{fontSize: 24, fontWeight: 'bold'}}>{title}</FormattedText>
            <Assignees assignees={_.get(issue, 'assignees')}/>
            <Labels/>
        </View>
    );
}

function Description({issue}) {
    const {description, web_url} = issue;
    const baseUrl = (web_url.match(/(.+)\/issues\/\d+/) || [])[1];

    if (!description) {
        return <AppContainer/>;
    }

    return <Markdown text={description} baseUrl={baseUrl}/>;
}

function Milestone({milestone}) {
    if (_.isEmpty(milestone)) {
        return null;
    }

    const {title, due_date} = milestone;

    return (
        <View>
            <FormattedText>{`Milestone: ${title}`}</FormattedText>
            {due_date && <FormattedText>{`Due Date: ${getBasicDateFormat(due_date)}`}</FormattedText>}
        </View>
    );
}

const Issue = connect(state => ({
    me: getMe(state),
    issue: getIssue(state),
    repository: getRepository(state)
}), {
    requestIssue, editIssue, navigate, setHeaderRightButton, setFooter
})(IssueComponent);
registerRoute({Issue});
