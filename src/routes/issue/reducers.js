import {registerReducer} from 'store';

function issue() {
    return {
        ISSUE_RECEIVED: (state, {data}) => ({...state, issue: data}),
        ISSUE_DISCUSSIONS_RECEIVED: (state, {data}) => ({...state, discussions: data})
    };
}

registerReducer('issue', issue());
