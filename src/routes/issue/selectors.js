import _ from 'lodash';
import {createSelector} from 'reselect';

const getIssueSelector = state => _.get(state, 'issue');
export const getIssue = createSelector(getIssueSelector, issue => _.get(issue, 'issue'));
export const getDiscussions = createSelector(getIssueSelector, issue => _.get(issue, 'discussions'));
