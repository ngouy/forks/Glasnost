import React from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {navigate} from 'router';
import _ from 'lodash';

import {colors, FormattedText, Icon} from 'elements';
import {SelectedIcon} from 'headerFooter';

import {getRepository} from './selectors';
import {getOpenMergeRequests} from '../mergeRequests/selectors';
import {getBranches} from '../branches/selectors';
import {getThemeValue} from '../settings/selectors';

function getBubbleStyle(darkTheme) {
    return {
        top: -12,
        right: 4,
        padding: 5,
        height: 26,
        borderRadius: 13,
        borderWidth: 1,
        borderColor: darkTheme ? colors.mainText : colors.backgroundColor,
        backgroundColor: darkTheme ? colors.mainText : colors.backgroundColor,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute'
    };
}

function IconWithCounterComponent({counter, route, children, darkTheme}) {
    return (
        <SelectedIcon route={route}>
            {!!counter
            && <View style={getBubbleStyle(darkTheme)}>
                <FormattedText opposite={true}>{counter}</FormattedText>
            </View>}
            {children}
        </SelectedIcon>
    );
}

function FooterComponent({repository = {}, mrs, branches, navigate}) {
    const {statistics, open_issues_count} = repository;
    const commit_count = _.get(statistics, 'commit_count', 0);

    return (
        <View style={{flexDirection: 'row', height: 45, flex: 1}}>
            <IconWithCounter counter={open_issues_count} route={'Issues'}>
                <Icon name={'issues'} size={30} onPress={() => navigate('Issues')}/>
            </IconWithCounter>
            <IconWithCounter counter={commit_count} route={'Commits'}>
                <Icon name={'commit'} size={30} onPress={() => navigate('Commits')}/>
            </IconWithCounter>
            <IconWithCounter counter={_.size(mrs)} route={'MergeRequests'}>
                <Icon name={'merge-request'} size={30} onPress={() => navigate('MergeRequests')}/>
            </IconWithCounter>
            <IconWithCounter counter={_.size(branches)} route={'Branches'}>
                <Icon name={'branch'} size={30} onPress={() => navigate('Branches')}/>
            </IconWithCounter>
            <IconWithCounter route={'FileList'}>
                <Icon name={'folder-o'} size={30} onPress={() => navigate('FileList')}/>
            </IconWithCounter>
            <IconWithCounter route={'Wikis'}>
                <Icon name={'book'} size={30} onPress={() => navigate('Wikis')}/>
            </IconWithCounter>
        </View>
    );
}

const IconWithCounter = connect(state => ({darkTheme: getThemeValue(state)}))(IconWithCounterComponent);
export const Footer = connect(state => ({
    repository: getRepository(state),
    mrs: getOpenMergeRequests(state),
    branches: getBranches(state)
}), {navigate})(FooterComponent);
