import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {getPipeline} from './selectors';
import {getLiteExperienceValue} from '../settings/selectors';
import {StatusIndicator} from './StatusIndicator';

function PipelineInfoComponent({pipeline, isLite, onPress}) {
    if (_.isEmpty(pipeline) || isLite) {
        return null;
    }

    return <StatusIndicator status={pipeline.status} onPress={onPress}/>;
}

export const PipelineInfo = connect((state, ownProps) => {
    const repoId = _.get(ownProps, 'repository.id');
    const defaultBranch = _.get(ownProps, 'repository.default_branch');
    const requestedBranch = _.get(ownProps, 'branch', defaultBranch);

    return {
        pipeline: getPipeline(state, repoId, requestedBranch),
        isLite: getLiteExperienceValue(state)
    };
})(PipelineInfoComponent);
