import _ from 'lodash';

import {get} from 'api';

import {getJobs} from './selectors';
import {getLiteExperienceValue} from '../settings/selectors';

export function clearPipelines() {
    return dispatch => dispatch({type: 'CLEAR_PIPELINES'});
}
export function requestPipelines(repos) {
    return async (dispatch, getState) => {
        const isLite = getLiteExperienceValue(getState());

        if (isLite) {
            return;
        }

        for (const repo of repos) {
            const {data} = await get({
                path: `projects/${repo.id}/pipelines`,
                silent: true,
                loader: false,
                pagination: false,
                additionalParams: {ref: repo.default_branch}
            });

            if (_.size(data)) {
                dispatch({type: 'PIPELINES_RECEIVED', data, projectId: repo.id});
            }
        }
    };
}

export function requestPipelinesForBranch(projectId, branch) {
    return async (dispatch, getState) => {
        const isLite = getLiteExperienceValue(getState());

        if (isLite) {
            return;
        }

        const {data} = await get({
            path: `projects/${projectId}/pipelines`,
            silent: true,
            loader: false,
            pagination: false,
            additionalParams: {ref: branch}
        });

        if (_.size(data)) {
            dispatch({
                type: 'PIPELINES_RECEIVED',
                data,
                projectId
            });
        }
    };
}

export function requestJobsForPipeline(projectId, pipelineId, forceReload = false) {
    return async (dispatch, getState) => {
        if (!pipelineId) {
            return null;
        }
        if (!forceReload) {
            const alreadyExistingJobs = getJobs(getState(), pipelineId);

            if (alreadyExistingJobs) {
                return null;
            }
        }

        const jobs = await get({path: `projects/${projectId}/pipelines/${pipelineId}/jobs`});

        if (_.size(_.get(jobs, 'data'))) {
            dispatch({
                type: 'JOBS_RECEIVED',
                pipelineId,
                data: _.map(jobs.data, (_job) => {
                    const job = _job;
                    job.projectId = projectId;
                    job.pipelineId = pipelineId;

                    return job;
                })
            });
        }
    };
}
