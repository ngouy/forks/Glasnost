import React from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

import {getLastActivity} from 'utils';
import {navigate} from 'router';
import {CircledImage, FormattedText, Tag} from 'elements';

import {getMergeRequest} from './selectors';

function HeaderComponent({mergeRequest, navigate}) {
    const {title, updated_at, labels, author, created_at} = mergeRequest;
    const authorName = _.get(author, 'name');
    const authorId = _.get(author, 'id');
    const authorUrl = _.get(author, 'avatar_url');
    const formattedCreatedAt = getLastActivity(created_at);
    const formattedLastActivity = getLastActivity(updated_at);

    return (
        <View style={{alignItems: 'center', margin: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                <FormattedText style={{fontSize: 14, marginRight: 10}}>{`Opened: ${formattedCreatedAt} by`}</FormattedText>
                <CircledImage name={authorName} url={authorUrl} onPress={() => navigate('Profile', {id: authorId})}/>
            </View>
            <FormattedText style={{fontSize: 20, fontWeight: 'bold'}}>{title}</FormattedText>
            <FormattedText style={{fontSize: 14}}>{`Last activity: ${formattedLastActivity}`}</FormattedText>
            <View style={{flexDirection: 'row', marginVertical: 20, justifyContent: 'space-around'}}>
                {_.map(labels, (label, index) => <Tag key={index} text={label}/>)}
            </View>
        </View>
    );
}

export const Header = connect(state => ({mergeRequest: getMergeRequest(state)}), {navigate})(HeaderComponent);
