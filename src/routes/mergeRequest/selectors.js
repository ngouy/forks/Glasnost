import _ from 'lodash';
import {createSelector} from 'reselect';

const getMergeRequestSelector = state => _.get(state, 'mergeRequest');
export const getMergeRequest = createSelector(getMergeRequestSelector, mergeRequest => _.get(mergeRequest, 'mergeRequest'));
export const getDiscussions = createSelector(getMergeRequestSelector, mergeRequest => _.get(mergeRequest, 'discussions'));
export const getThumbs = createSelector(getMergeRequestSelector, mergeRequest => _.get(mergeRequest, 'thumbs'));
export const getDiffs = createSelector(getMergeRequestSelector, mergeRequest => _.get(mergeRequest, 'diffs'));
