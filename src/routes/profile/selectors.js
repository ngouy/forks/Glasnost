import _ from 'lodash';
import {createSelector} from 'reselect';

const getProfile = state => _.get(state, 'profile');
export const getMe = createSelector(getProfile, profile => _.get(profile, 'profile'));
export const getOtherGuy = createSelector(getProfile, profile => _.get(profile, 'otherGuyProfile'));
export const getEvents = createSelector(getProfile, profile => _.get(profile, 'events'));
export const getOtherGuyEvents = createSelector(getProfile, profile => _.get(profile, 'otherGuyEvents'));
