import React from 'react';
import {Switch} from 'react-native';
import {connect} from 'react-redux';

import {Card, FormattedText} from 'elements';
import {setData} from 'utils';

import {getThemeValue} from './selectors';
import {setTheme} from './actions';

function DarkThemeComponent({darkTheme, setTheme}) {
    return (
        <Card style={{content: {flexDirection: 'row', justifyContent: 'space-between'}}} title={'Theme'} icon={{name: 'appearance'}}>
            <FormattedText style={{fontSize: 18}}>Dark Theme:</FormattedText>
            <Switch value={darkTheme} onValueChange={() => {
                setData('darkTheme', String(!darkTheme));
                setTheme(String(!darkTheme));
            }}/>
        </Card>
    );
}

export const DarkTheme = connect(state => ({darkTheme: getThemeValue(state)}), {setTheme})(DarkThemeComponent);
