import React, {memo, Component} from 'react';
import {View} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';

import {getLastUpdatedTime, getProjectPermission} from 'utils';
import {FormattedText, Icon, Markdown, Tag} from 'elements';

import {PipelineInfo} from '../pipelines/PipelineInfo';
import {Jobs} from '../pipelines';
import {getPipeline} from '../pipelines/selectors';
import {requestJobsForPipeline} from '../pipelines/actions';

function IconInfos({repository}) {
    const {visibility, star_count, forks_count} = repository;

    return (
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{marginRight: 10, flexDirection: 'row'}}>
                <Icon name={'star'} size={20}/>
                <FormattedText>{star_count}</FormattedText>
            </View>
            <View style={{marginRight: 10, flexDirection: 'row'}}>
                <Icon name={'fork'} size={20}/>
                <FormattedText>{forks_count}</FormattedText>
            </View>
            <Icon name={visibility === 'private' ? 'lock' : 'lock-open'} size={20}/>
        </View>
    );
}

class DescriptionComponent extends Component {
    state = {areJobsVisible: false};

    onPipelinePress() {
        const {areJobsVisible} = this.state;
        const {repository, requestJobsForPipeline, pipeline = {}} = this.props;

        if (!areJobsVisible) {
            requestJobsForPipeline(repository.id, pipeline.id);
        }

        this.setState({areJobsVisible: !areJobsVisible});
    }

    render() {
        const {repository, pipeline = {}} = this.props;
        const {areJobsVisible} = this.state;

        const {name_with_namespace, last_activity_at, description, permissions} = repository;
        const {unit, value} = getLastUpdatedTime(last_activity_at);
        const project_access = _.get(permissions, 'project_access', {});
        const group_access = _.get(permissions, 'group_access', {});
        const permission = getProjectPermission(project_access, group_access);

        return (
            <View style={{paddingHorizontal: 15, flex: 1}}>
                <FormattedText style={{fontWeight: 'bold'}}>{name_with_namespace}</FormattedText>
                {!!description && <Markdown
                    text={description}
                    style={{text: {fontSize: 14, fontStyle: 'italic'}}}/>}
                <FormattedText>{`Last update: ${value} ${unit} ago`}</FormattedText>

                <View style={{marginVertical: 5, flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View style={{flex: 0.9, flexDirection: 'row'}}>
                        <IconInfos repository={repository}/>
                        {!!permission && <Tag text={permission}/>}
                    </View>
                    <View style={{flex: 0.1}}>
                        <PipelineInfo repository={repository} onPress={() => this.onPipelinePress()}/>
                    </View>
                </View>
                {areJobsVisible
                    && <View style={{marginVertical: 5}}>
                        <Jobs pipelineId={pipeline.id}/>
                    </View>
                }
            </View>
        );
    }
}

export const Description = connect((state, {repository: {id, default_branch}}) => ({pipeline: getPipeline(state, id, default_branch)}),
                                        {requestJobsForPipeline})(memo(DescriptionComponent));
