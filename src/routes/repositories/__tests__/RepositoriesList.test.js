import React from 'react';
import {Provider} from 'react-redux';

import {getRouteName} from 'router';
import {getPagination} from 'utils';
import {getHeaderLeftButton, getHeaderRightButton} from 'headerFooter/selectors';
import {clearRepositories, setLocalSearch} from '../actions';

const fakeRepositories = [
    {id: 1, visibility: 'private'},
    {id: 2, visibility: 'public', description: 'someDescription', project_access: 30}
];

describe('RepositoriesList', () => {
    let RepositoriesList;
    let store;

    beforeAll(() => {
        const {setStore} = require('api');
        const {getStore} = require('store');

        require('../index');
        setStore(getStore());
    });

    beforeEach(() => {
        RepositoriesList = require('../RepositoriesList').RepositoriesList;

        store = require('store').getStore();
        store.dispatch({type: 'REPOSITORIES_RECEIVED', data: fakeRepositories});
        store.dispatch({type: 'USER_RECEIVED', data: {name: 'someName'}});
        store.dispatch(setLocalSearch(false));
    });

    it('renders correctly the empty page', async () => {
        store.dispatch(clearRepositories());
        const component = await asyncCreate(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('renders correctly the page', async () => {
        const component = await asyncCreate(<Provider store={store}><RepositoriesList entityName={'repositories'} repositories={fakeRepositories}/></Provider>);

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('clicking on a single repository should navigate to Repository', async () => {
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'} repositories={fakeRepositories}/></Provider>);
        await waitForAsync();
        component.update();

        const SingleRepository = component.find('SingleRepositoryComponent').at(0).find('ItemListComponent');
        SingleRepository.props().onPress();

        expect(getRouteName(store.getState())).toEqual('Repository');
    });

    it('clicking on a pipeline should show the related jobs', async () => {
        store.dispatch({type: 'PIPELINES_RECEIVED', projectId: 1, data: [{ref: 'someBranch', id: 1, status: 'running'}]});
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'} repositories={fakeRepositories}/></Provider>);
        await waitForAsync();
        component.update();

        const PipelineInfo = component.find('PipelineInfoComponent').at(0);
        PipelineInfo.props().onPress();

        expect(component.html()).toMatchSnapshot();
    });

    it('should call the functions when unmounting', async () => {
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);

        component.unmount();
        await waitForAsync();

        expect(getPagination(store.getState(), 'repositories')).toMatchSnapshot();
    });

    it('should toggle the search bar for the local repositories', async () => {
        store.dispatch(setLocalSearch(true));
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should write something on the search bar', async () => {
        store.dispatch(setLocalSearch(true));
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        const SearchBar = component.find('SearchComponent').find('TextInput').at(0);
        SearchBar.props().onChangeText('someMessage2');
        SearchBar.props().value = 'someMessage2';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should write something on the other bar', async () => {
        store.dispatch(setLocalSearch(false));
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        const SearchBar = component.find('SearchComponent').find('TextInput').at(1);
        SearchBar.props().onChangeText('someMessage2');
        SearchBar.props().value = 'someMessage2';
        component.update();

        expect(component.html()).toMatchSnapshot();
    });

    it('should toggle the search bar for the online repositories', async () => {
        store.dispatch(setLocalSearch(false));
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        expect(component.html()).toMatchSnapshot();

        component.unmount();
    });

    it('should toggle the local and remote search', async () => {
        store.dispatch(setLocalSearch(false));
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);
        await waitForAsync();
        component.update();

        const SearchButton = getHeaderRightButton(store.getState());
        SearchButton.props.onPress();
        component.update();

        const LocalSearch = component.find('SearchComponent').find('TouchableOpacity').at(0);
        LocalSearch.props().onPress();
        component.update();
        await waitForAsync();

        const RemoteSearch = component.find('SearchComponent').find('TouchableOpacity').at(1);
        RemoteSearch.props().onPress();
        component.update();
        await waitForAsync();

        expect(component.html()).toMatchSnapshot();
    });

    it('should show MyFace', async () => {
        const component = mount(<Provider store={store}><RepositoriesList entityName={'repositories'}/></Provider>);
        await waitForAsync();
        component.update();

        const MyFace = getHeaderLeftButton(store.getState());

        expect(MyFace).toMatchSnapshot();
    });
});
