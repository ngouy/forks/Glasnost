import _ from 'lodash';

import {get} from 'api';

import {getRepository} from '../repository/selectors';

export function requestWiki(slug) {
    return async (dispatch, getState) => {
        const repo = getRepository(getState());

        if (_.isEmpty(repo)) {
            return;
        }

        const {data} = await get({path: `projects/${repo.id}/wikis/${slug}`});

        if (data) {
            dispatch({type: 'WIKI_RECEIVED', data});
        }
    };
}
