import App from './App';

describe('App', () => {
    it('should render the whole screen', () => {
         expect(App()).toMatchSnapshot();
    });
});