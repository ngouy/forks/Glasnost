import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {create} from "react-test-renderer";

import {loadConfig} from 'api';

const axiosMock = new MockAdapter(axios);

process.env.NODE_ENV = 'test';

loadConfig('https://localhost', 'token', 'authType');

Enzyme.configure({adapter: new Adapter()});

jest.mock('react-native-fingerprint-scanner/src', () => ({isSensorAvailable: () => 'Fingerprint'}));

jest.mock('react-native-keychain', () => ({
    getGenericPassword:   jest.fn().mockReturnValue({password: JSON.stringify({userToken:"[{\"accessToken\":\"someToken\", \"type\":\"token\", \"url\": \"https://someGitlabUrl.com\"}]"})}),
    setGenericPassword:   jest.fn(),
    resetGenericPassword: jest.fn()
}));

jest.mock('utils', () => ({
    ...require.requireActual('utils'),
    getData: jest.fn(),
    setData: jest.fn(),
    showMessage: jest.fn()
}));

global.mount = mount;
global.waitForAsync = () => new Promise(resolve => setImmediate(resolve));
global.asyncCreate = async component => {
    const tree = create(component);
    await waitForAsync();

    return tree;
};

beforeEach(() => {
    global.onGet = ({url, data, status = 200}) => axiosMock.onGet(url).reply(status, data, {'x-page': 1, 'x-per-page': 20});
    axiosMock.reset();
});

function suppressDomErrors() {
    const suppressedErrors = /(Warning.*not wrapped in act|Warning: Received `%s`|Warning: Invalid value for prop %s on <%s> tag|React does not recognize the.*prop on a DOM element|Unknown event handler property|is using uppercase HTML|Received `true` for a non-boolean attribute `accessible`|The tag.*is unrecognized in this browser|PascalCase)/;
    // eslint-disable-next-line no-console
    const realConsoleError = console.error;
    // eslint-disable-next-line no-console
    console.error = message => {
        if (message.match(suppressedErrors)) {
            return;
        }
        realConsoleError(message);
    };
}

suppressDomErrors();